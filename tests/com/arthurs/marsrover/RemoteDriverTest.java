package com.arthurs.marsrover;

import static org.junit.Assert.*;

import org.junit.Test;

public class RemoteDriverTest {

	@Test
	public void testSendCommands() {
		
		RoverPositions position = new RoverPositions(5,5,1);
		Rover rover = new Rover(position);
		CommandReceiver commandReceiver = new CommandReceiver(rover);
		
		RemoteDriver remoteDriver = new RemoteDriver(commandReceiver); // assert that actual and
		//expected are same assertEquals(expected, actual);
		remoteDriver.sendCommands("F,F,F,B,B,B");
		
		//assertThat(remoteDriver.sendCommands("F,F,F,B,B,B"), is(expected));
		
	}

}
