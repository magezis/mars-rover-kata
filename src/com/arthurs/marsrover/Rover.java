package com.arthurs.marsrover;


/**
 * Mars Rover Object
 * @author arthur
 *
 */
public class Rover {
	
	/** 
	 * we now receive the positions from this object
	 */
	private RoverPositions position;
	
	/**
	 * checking inital state and changing to new
	 * depending on the new commands received
	 */
	private RoverState facingState;
	
	/**
	 * Taking into account states(Initial) before move
	 */
	private final RoverState NORTH_STATE;
	private final RoverState EAST_STATE;
	private final RoverState SOUTH_STATE;
	private final RoverState WEST_STATE;
	
	
	/**
	 * With this position we know where to move
	 * @param position
	 */
	 public Rover(RoverPositions position) {
	    this.NORTH_STATE = new NorthState(this);
	    this.EAST_STATE = new EastState(this);
	    this.SOUTH_STATE = new SouthState(this);
	    this.WEST_STATE = new WestState(this);

	    this.facingState = getNorthState();
	    this.position = position;
	  }


	  public RoverState getFacingState() {
	    return facingState;
	  }

	  public void setFacingState(RoverState facingState) {
	    this.facingState = facingState;
	  }

	  
	  
	  /**
	   * forward move state
	   */
	  public void moveForward() {
	    facingState.moveForward();
	  }
	  /**
	   * backward move state
	   */
  	public void moveBackward() {
  		facingState.moveBackward();
  	}
  
	public RoverState getNorthState() {
		// TODO Auto-generated method stub
	    return NORTH_STATE;
	}
	  
	public RoverState getEastState() {
		// TODO Auto-generated method stub
	    return EAST_STATE;
	}
	
	public RoverState getSouthState() {
		// TODO Auto-generated method stub
	    return SOUTH_STATE;
	}
	public RoverState getWestState() {
		// TODO Auto-generated method stub
		return WEST_STATE;
	}
	
	
	
	public void incrementY() {
		// TODO Auto-generated method stub
		this.position.incrementX();
	}
	
	
	public void incrementX() {
		// TODO Auto-generated method stub
		this.position.incrementY();
	}
	
	
	public void decrementX() {
	   this.position.decrementX();
	}
	
	public void decrementY() {
	   this.position.decrementY();
	}

	 
	 
	 
}


