package com.arthurs.marsrover.rovercommands;

import com.arthurs.marsrover.Rover;

/**
 * Backward command imolementation
 * @author arthur
 *
 */
public class MoveBackwardCommand implements RoverMotionCommand {

	  private Rover rover;
	
	  public MoveBackwardCommand(Rover rover) {
	    this.rover = rover;
	  }
	
	
	  public void execute() {
	    rover.moveBackward();
	  }
	  
	  
}