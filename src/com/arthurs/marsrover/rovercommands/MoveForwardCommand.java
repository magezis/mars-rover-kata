package com.arthurs.marsrover.rovercommands;

import com.arthurs.marsrover.Rover;


/**
 * Forward command imolementation
 * @author arthur
 *
 */
public class MoveForwardCommand  implements RoverMotionCommand{

	  private Rover rover;

	  public MoveForwardCommand(Rover rover) {
	    this.rover = rover;
	  }


	  public void execute() {
	    rover.moveForward();
	  }
}