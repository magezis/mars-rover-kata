package com.arthurs.marsrover.rovercommands;

public interface RoverMotionCommand {

  void execute();
  
  
}