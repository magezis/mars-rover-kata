package com.arthurs.marsrover;



public class WestState implements RoverState {

  private Rover rover;

  public WestState(Rover rover) {
    this.rover = rover;
  }

  @Override
  public void moveForward() {
    rover.decrementY();
  }

  @Override
  public void moveBackward() {
    rover.incrementY();
  }
  
  
}