package com.arthurs.marsrover;



public class SouthState implements RoverState {

  private Rover rover;

  public SouthState(Rover rover) {
    this.rover = rover;
  }

  @Override
  public void moveForward() {
    rover.decrementX();
  }

  @Override
  public void moveBackward() {
    rover.incrementX();
  }
  
}