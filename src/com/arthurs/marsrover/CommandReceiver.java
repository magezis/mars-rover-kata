package com.arthurs.marsrover;

import java.util.HashMap;
import java.util.Map;

import com.arthurs.marsrover.rovercommands.MoveBackwardCommand;
import com.arthurs.marsrover.rovercommands.MoveForwardCommand;
import com.arthurs.marsrover.rovercommands.RoverMotionCommand;

/**
 * Receives the commands from earth
 * @author arthur
 *
 */
public class CommandReceiver {
	
	private Rover rover;
	  private Map<Character, RoverMotionCommand> commands;

	  public CommandReceiver(Rover rover) {
	    this.rover = rover;
	    initializeAvailableCommands();
	  }

	  public Map<Character, RoverMotionCommand> availableCommands() {
	    return commands;
	  }
	  
	  
	  public RoverMotionCommand getCommandByCode(char commandCode) {
		    if (commandIsAvailable(commandCode)) {
		      return retrieveCommandByCode(commandCode);
		    }
	    return null;
	  }


	  private void initializeAvailableCommands() {
		    commands = new HashMap<>();

		    commands.put('F', new MoveForwardCommand(rover));
		    commands.put('B', new MoveBackwardCommand(rover));
	}
	  private boolean commandIsAvailable(char commandCode) {
	    return commands.containsKey(commandCode);
	  }

	  private RoverMotionCommand retrieveCommandByCode(char commandCode) {
	    return commands.get(commandCode);
	  }
	
	
	

}
