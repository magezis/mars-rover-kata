package com.arthurs.marsrover;



public class EastState implements RoverState {

  private Rover rover;

  public EastState(Rover rover) {
    this.rover = rover;
  }

  @Override
  public void moveForward() {
    rover.incrementY();
  }

  @Override
  public void moveBackward() {
    rover.decrementY();
  }
  
  
  
}