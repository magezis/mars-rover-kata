package com.arthurs.marsrover;

/**
 * Takes care of the different
 * directions or movements of the Rover
 * @author arthur
 *
 */
public class RoverPositions {
	
	private int x;
	private int y;
	private int unitMove = 1;
	
	
	/**
	 * From knowing our initial position
	 * we can make a unit move either on
	 * command directions base on the 
	 * command
	 * @param x
	 * @param y
	 * @param unitMove
	 */
	public RoverPositions(int x, int y, int unitMove) {
	    this.x = x;
	    this.y = y;
	    this.unitMove = unitMove;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getUnitMove() {
		return unitMove;
	}

	public void setUnitMove(int unitMove) {
		this.unitMove = unitMove;
	}
	
	public RoverPositions incrementX() {
	    x = (x + unitMove);
	    return this;
	}

	public RoverPositions decrementX() {
	    x = (x - unitMove);
	    return this;
	}

	public RoverPositions incrementY() {
	    y = (y + unitMove);
	    return this;
	}

	public RoverPositions decrementY() {
	    y = (y - unitMove);
	    return this;
	}
	
	@Override
	public String toString() {
		return ""+x+y;
	}

	

}
