package com.arthurs.marsrover;



public class NorthState implements RoverState {

  private Rover rover;

  public NorthState(Rover rover) {
    this.rover = rover;
  }

  @Override
  public void moveForward() {
    rover.incrementX();
  }

  @Override
  public void moveBackward() {
    rover.decrementX();
  }
  
}