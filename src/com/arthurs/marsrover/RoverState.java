package com.arthurs.marsrover;

public interface RoverState {

  void moveForward();

  void moveBackward();
}