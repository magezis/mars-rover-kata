package com.arthurs.marsrover;

import com.arthurs.marsrover.rovercommands.RoverMotionCommand;

public class RemoteDriver {

  private CommandReceiver commandReceiver;

  public RemoteDriver(CommandReceiver commandReceiver) {
    this.commandReceiver = commandReceiver;
  }

  public void sendCommands(String commands) {
    for (char commandCode : commands.toCharArray()) {
      RoverMotionCommand command = commandReceiver.getCommandByCode(commandCode);
      command.execute();
    }
  }
  
  public static void main(String[] args) {
	  RoverPositions position = new RoverPositions(5,5,1);
		Rover rover = new Rover(position);
		CommandReceiver commandReceiver = new CommandReceiver(rover);
		
		RemoteDriver remoteDriver = new RemoteDriver(commandReceiver); // assert that actual and
		//expected are same assertEquals(expected, actual);
		remoteDriver.sendCommands("FFFBBB");
  }
  
  
  
  
}