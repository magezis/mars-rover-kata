package com.arthurs.marsrover;


/**
 * implements the moves of the Rover Depending on the 
 * @author arthur
 *
 */
public class RoverMovement {
	/**
	 * Rover to be moved
	 */
	private Rover rover;
	
	/**
	 * Contructor that takes command 
	 * and roverto be acted on
	 */
	public RoverMovement(Rover rover) {
		this.rover = rover;
	}
}
